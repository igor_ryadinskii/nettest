﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using ServiceStack.DataAnnotations;
using System;

namespace NetTests.domains
{
    public class BookMongo //: IEntity
    {

        [BsonId]
        public ObjectId _id { get; set; }
        public int publicYear { get; set; }
        public int pageCount { get; set; }
        public string title { get; set; }
        public ObjectId author { get; set; }
    }
}