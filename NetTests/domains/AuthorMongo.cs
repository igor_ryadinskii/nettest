﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace NetTests.domains
{
    public class AuthorMongo //: IEntity
    {
        [BsonId]
        public ObjectId _id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string familyName { get; set; }
    }
}