﻿using ServiceStack.DataAnnotations;
using System;

namespace NetTests.domains
{
    public class BookSql
    {
        [AutoIncrement]
        public Int32 id { get; set; }
        public int publicYear { get; set; }
        public int pageCount { get; set; }
        public string title { get; set; }
        public string author { get; set; }
    }
}