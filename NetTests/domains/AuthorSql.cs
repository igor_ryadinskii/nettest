﻿using ServiceStack.DataAnnotations;
using System;

namespace NetTests.domains
{
    public class AuthorSql
    {
        [AutoIncrement]
        public Int32 id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string familyName { get; set; }
    }
}