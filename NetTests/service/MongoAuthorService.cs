﻿using MongoDB.Bson;
using MongoDB.Driver;
using NetTests.Controllers;
using NetTests.domains;
using NetTests.statuses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NetTests.service
{
    public class MongoAuthorService : IDbService<AuthorDTO>
    {
        private IMongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<AuthorMongo> collection;
        private readonly CancellationTokenSource timeoutCancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

        public MongoAuthorService()
        {
            client = new MongoClient();
            database = client.GetDatabase("nettest");
            collection = database.GetCollection<AuthorMongo>("author");
        }

        public async Task Delete(string id)
        {
            try
            {
                var filter = Builders<AuthorMongo>.Filter.Eq("_id", ObjectId.Parse(id));
                await collection.DeleteOneAsync(filter, timeoutCancellationTokenSource.Token);
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Author with id {0} not found", id));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<AuthorDTO> FindById(string Id)
        {
            var filter = Builders<AuthorMongo>.Filter.Eq("_id", ObjectId.Parse(Id));
            try
            {
                AuthorMongo author = await collection.Find<AuthorMongo>(filter).SingleAsync(timeoutCancellationTokenSource.Token);
                return new AuthorDTO()
                {
                    id = author._id.ToString(),
                    familyName = author.familyName,
                    firstName = author.firstName,
                    secondName = author.secondName
                };
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Author with id {0} not found", Id));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAll(int limit, int offset)
        {
            try
            {
                ResponseListAuthor response = new ResponseListAuthor();
                var authors = await collection.Find(FilterDefinition<AuthorMongo>.Empty).ToListAsync(timeoutCancellationTokenSource.Token);
                response.count = authors.Count();
                authors = authors.Skip(offset).Take(limit).ToList();
                List<AuthorDTO> resultDTO = new List<AuthorDTO>();
                foreach (AuthorMongo author in authors)
                {
                    resultDTO.Add(
                        new AuthorDTO()
                        {
                            id = author._id.ToString(),
                            familyName = author.familyName,
                            firstName = author.firstName,
                            secondName = author.secondName
                        });
                }
                response.entities = resultDTO;
                return response;
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(List<AuthorDTO> entitiesList)
        {
            try
            {
                List<AuthorMongo> authorsList = new List<AuthorMongo>();
                foreach (AuthorDTO author in entitiesList)
                {
                    authorsList.Add(
                    new AuthorMongo()
                    {
                        _id = ObjectId.Empty,
                        familyName = author.familyName,
                        firstName = author.firstName,
                        secondName = author.secondName
                    });
                }
                await collection.InsertManyAsync(authorsList, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(AuthorDTO entity)
        {
            try
            {
                AuthorMongo author = new AuthorMongo()
                {
                    _id = ObjectId.Empty,
                    familyName = entity.familyName,
                    firstName = entity.firstName,
                    secondName = entity.secondName
                };
                await collection.InsertOneAsync(author, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Update(AuthorDTO entity, string id)
        {
            try
            {
                AuthorMongo author = new AuthorMongo()
                {
                    _id = ObjectId.Parse(id),
                    familyName = entity.familyName,
                    firstName = entity.firstName,
                    secondName = entity.secondName
                };
                var filter = Builders<AuthorMongo>.Filter.Eq("_id", ObjectId.Parse(id));
                await collection.ReplaceOneAsync(filter, author, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Author with id {0} not found", id));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public Task<BaseResponse> getAuthorBook(int limit, int offset, string authorId)
        {
            throw new NotImplementedException();
        }

    }
}