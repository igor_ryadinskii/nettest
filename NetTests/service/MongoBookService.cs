﻿using MongoDB.Bson;
using MongoDB.Driver;
using NetTests.Controllers;
using NetTests.domains;
using NetTests.statuses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace NetTests.service
{
    public class MongoBookService : IDbService<BookDTO>
    {
        private IMongoClient client;
        private IMongoDatabase database;
        private IMongoCollection<BookMongo> collection;
        private readonly CancellationTokenSource timeoutCancellationTokenSource = new CancellationTokenSource(TimeSpan.FromSeconds(10));

        public MongoBookService()
        {
            client = new MongoClient();
            database = client.GetDatabase("nettest");
            collection = database.GetCollection<BookMongo>("book");
        }

        public async Task Delete(string id)
        {
            try
            {
                var filter = Builders<BookMongo>.Filter.Eq("_id", ObjectId.Parse(id));
                await collection.DeleteOneAsync(filter, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Book with id {0} not found", id));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BookDTO> FindById(string Id)
        {
            try
            {
                var filter = Builders<BookMongo>.Filter.Eq("_id", ObjectId.Parse(Id));
                BookMongo book = await collection.Find<BookMongo>(filter).SingleAsync(cancellationToken: timeoutCancellationTokenSource.Token);
                return new BookDTO()
                {
                    id = book._id.ToString(),
                    pageCount = book.pageCount,
                    publicYear = book.publicYear,
                    title = book.title,
                    author = DbManager.getCurrentDbServiceAuthor().FindById(book.author.ToString()).Result
                };
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Book with id {0} not found", Id));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAll(int limit, int offset)
        {
            try
            {
                ResponseListBook response = new ResponseListBook();
                var books = await collection.Find(FilterDefinition<BookMongo>.Empty).ToListAsync(cancellationToken: timeoutCancellationTokenSource.Token);
                response.count = books.Count();
                books = books.Skip(offset).Take(limit).ToList();
                List<BookDTO> resultBooks = new List<BookDTO>();
                foreach (BookMongo book in books)
                {
                    AuthorDTO authorOfBook = null;
                    try
                    {
                        authorOfBook = DbManager.getCurrentDbServiceAuthor().FindById(book.author.ToString()).Result;
                    }
                    catch { }
                    resultBooks.Add(
                        new BookDTO()
                        {
                            id = book._id.ToString(),
                            pageCount = book.pageCount,
                            publicYear = book.publicYear,
                            title = book.title,
                            author = authorOfBook
                        });
                }
                response.entities = resultBooks;
                return response;
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(List<BookDTO> entitiesList)
        {
            try
            {
                List<BookMongo> booksList = new List<BookMongo>();
                foreach (BookDTO book in entitiesList)
                {
                    booksList.Add(
                        new BookMongo()
                        {
                            _id = ObjectId.Empty,
                            author = ObjectId.Parse(book.author.id),
                            pageCount = book.pageCount,
                            publicYear = book.publicYear,
                            title = book.title
                        });
                }
                await collection.InsertManyAsync(booksList, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(BookDTO entity)
        {
            try
            {
                await collection.InsertOneAsync(new BookMongo()
                {
                    _id = ObjectId.Empty,
                    author = ObjectId.Parse(entity.author.id),
                    pageCount = entity.pageCount,
                    publicYear = entity.publicYear,
                    title = entity.title
                }, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Update(BookDTO entity, string id)
        {
            try
            {
                BookMongo book = new BookMongo()
                {
                    _id = ObjectId.Parse(id),
                    author = ObjectId.Parse(entity.author.id),
                    pageCount = entity.pageCount,
                    publicYear = entity.publicYear,
                    title = entity.title
                };
                var filter = Builders<BookMongo>.Filter.Eq("_id", ObjectId.Parse(id));
                await collection.ReplaceOneAsync(filter, book, cancellationToken: timeoutCancellationTokenSource.Token);
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Book with id {0} not found", id));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAuthorBook(int limit, int offset, string authorId)
        {
            try
            {
                var filter = Builders<BookMongo>.Filter.Eq("author", ObjectId.Parse(authorId));
                ResponseListBook response = new ResponseListBook();
                var books = await collection.Find(filter).ToListAsync(cancellationToken: timeoutCancellationTokenSource.Token);
                response.count = books.Count();
                books = books.Skip(offset).Take(limit).ToList();
                List<BookDTO> resultBooks = new List<BookDTO>();
                foreach (BookMongo book in books)
                {
                    resultBooks.Add(
                        new BookDTO()
                        {
                            id = book._id.ToString(),
                            pageCount = book.pageCount,
                            publicYear = book.publicYear,
                            title = book.title,
                            author = new AuthorDTO() { id = authorId }
                        });
                }
                response.entities = resultBooks;
                return response;
            }
            catch (InvalidOperationException)
            {
                throw new NotFoundException(String.Format("Author with id {0} not found", authorId));
            }
            catch (OperationCanceledException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }
    }
}