﻿using NetTests.Controllers;
using NetTests.domains;
using NetTests.statuses;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NetTests.service
{
    public class SqlBookService : IDbService<BookDTO>
    {
        private IDbConnection db;
        private OrmLiteConnectionFactory dbFactory;
        public SqlBookService()
        {
            OrmLiteConfig.CommandTimeout = 10000;
            dbFactory = new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["postgresql"].ConnectionString, PostgreSqlDialect.Provider);
            using (db = dbFactory.Open())
            {
                db.CreateTableIfNotExists<BookSql>();
            }
        }

        public async Task Delete(string id)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    await db.DeleteAsync<BookSql>(p => p.id == Int32.Parse(id));
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
}

        public async Task<BookDTO> FindById(string Id)
        {
            try
            {
                BookSql book = null;
                using (db = dbFactory.Open())
                {
                    book = await db.SingleAsync<BookSql>(Book => Book.id == Int32.Parse(Id));
                }
                if (book == null)
                    throw new NotFoundException(String.Format("Book with id {0} not found", Id));
                return new BookDTO()
                {
                    id = book.id.ToString(),
                    author = DbManager.getCurrentDbServiceAuthor().FindById(book.author).Result,
                    pageCount = book.pageCount,
                    publicYear = book.publicYear,
                    title = book.title
                };
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAll(int limit, int offset)
        {
            try
            {
                ResponseListBook response = new ResponseListBook();
                using (db = dbFactory.Open())
                {
                    response.count = await db.CountAsync<BookSql>();
                    var books = await db.SelectAsync<BookSql>();
                    books = books.Skip(offset).Take(limit).ToList();
                    List<BookDTO> responseBooks = new List<BookDTO>();
                    foreach (BookSql book in books)
                    {
                        AuthorDTO authorDTO = null;
                        try
                        {
                            authorDTO = DbManager.getCurrentDbServiceAuthor().FindById(book.author).Result;
                        }
                        catch { }
                        responseBooks.Add(new BookDTO()
                        {
                            id = book.id.ToString(),
                            author = authorDTO,
                            pageCount = book.pageCount,
                            publicYear = book.publicYear,
                            title = book.title
                        });
                    }
                    response.entities = responseBooks;
                }
                return response;
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(List<BookDTO> entitiesList)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    foreach (BookDTO book in entitiesList)
                        await db.SaveAsync(new BookSql()
                        {
                            author = book.author.id,
                            pageCount = book.pageCount,
                            publicYear = book.publicYear,
                            title = book.title
                        });
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(BookDTO book)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    await db.SaveAsync(new BookSql()
                    {
                        author = book.author.id,
                        pageCount = book.pageCount,
                        publicYear = book.publicYear,
                        title = book.title
                    });
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Update(BookDTO book, string id)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    await db.UpdateAsync<BookSql>(new BookSql()
                    {
                        id = Int32.Parse(id),
                        author = book.author.id,
                        pageCount = book.pageCount,
                        publicYear = book.publicYear,
                        title = book.title
                    });
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAuthorBook(int limit, int offset, string authorId)
        {
            try
            {
                ResponseListBook response = new ResponseListBook();
                using (db = dbFactory.Open())
                {
                    response.count = await db.CountAsync<BookSql>(Book => Book.author == authorId);
                    List<BookSql> books = await db.SelectAsync<BookSql>(Book => Book.author == authorId);
                    books = books.Skip(offset).Take(limit).ToList();
                    List<BookDTO> responseBooks = new List<BookDTO>();
                    foreach (BookSql book in books)
                    {
                        responseBooks.Add(new BookDTO()
                        {
                            id = book.id.ToString(),
                            pageCount = book.pageCount,
                            publicYear = book.publicYear,
                            title = book.title,
                            author = new AuthorDTO() { id = authorId }
                        });
                    }
                    response.entities = responseBooks;
                    return response;
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }
    }
}