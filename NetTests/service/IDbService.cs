﻿using NetTests.Controllers;
using NetTests.domains;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NetTests.service
{
    public interface IDbService<T> where T : IEntity
    {
        Task Insert(T entity);
        Task Insert(List<T> entitiesList);
        Task<BaseResponse> getAll(int limit, int offset);
        Task<BaseResponse> getAuthorBook(int limit, int offset, string authorId);
        Task Update(T entity, string id);
        Task Delete(string id);
        Task<T> FindById(string Id);
    }
}
