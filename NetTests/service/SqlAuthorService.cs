﻿using NetTests.Controllers;
using NetTests.domains;
using NetTests.statuses;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace NetTests.service
{
    public class SqlAuthorService : IDbService<AuthorDTO>
    {
        private IDbConnection db;
        private OrmLiteConnectionFactory dbFactory;
        
        public SqlAuthorService()
        {
            try
            {
                OrmLiteConfig.CommandTimeout = 10000;
                dbFactory = new OrmLiteConnectionFactory(ConfigurationManager.ConnectionStrings["postgresql"].ConnectionString, PostgreSqlDialect.Provider);
                using (db = dbFactory.Open())
                {
                    db.CreateTableIfNotExists<AuthorSql>();
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }
        
        public async Task Delete(string id)
        {
                using (db = dbFactory.Open())
                {
                    await db.DeleteAsync<AuthorSql>(p => p.id == Int32.Parse(id));
                }
        }

        public async Task<AuthorDTO> FindById(string Id)
        {
            try
            {
                AuthorSql author = null;
                using (db = dbFactory.Open())
                {
                    author = await db.SingleAsync<AuthorSql>(a => a.id == Int32.Parse(Id));
                }
                if (author == null)
                    throw new NotFoundException(String.Format("Author with id {0} not found", Id));
                return new AuthorDTO()
                {
                    id = author.id.ToString(),
                    familyName = author.familyName,
                    firstName = author.firstName,
                    secondName = author.secondName
                };
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAll(int limit, int offset)
        {
            try
            {
                ResponseListAuthor response = new ResponseListAuthor();
                using (db = dbFactory.Open())
                {
                    response.count = db.Count<AuthorSql>();
                    var authors = await db.SelectAsync<AuthorSql>();
                    authors = authors.Skip(offset).Take(limit).ToList();
                    List<AuthorDTO> responseAuthors = new List<AuthorDTO>();
                    foreach (AuthorSql author in authors)
                        responseAuthors.Add(
                            new AuthorDTO()
                            {
                                id = author.id.ToString(),
                                familyName = author.familyName,
                                firstName = author.firstName,
                                secondName = author.secondName
                            });
                    response.entities = responseAuthors;
                }
                return response;
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(List<AuthorDTO> entitiesList)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    foreach (AuthorDTO author in entitiesList)
                        await db.SaveAsync(new AuthorSql()
                        {
                            familyName = author.familyName,
                            firstName = author.firstName,
                            secondName = author.secondName
                        });
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Insert(AuthorDTO author)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    await db.SaveAsync(new AuthorSql()
                    {
                        familyName = author.familyName,
                        firstName = author.firstName,
                        secondName = author.secondName
                    });
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task Update(AuthorDTO author, string id)
        {
            try
            {
                using (db = dbFactory.Open())
                {
                    await db.UpdateAsync<AuthorSql>(new AuthorSql()
                    {
                        id = Int32.Parse(author.id),
                        familyName = author.familyName,
                        firstName = author.firstName,
                        secondName = author.secondName
                    });
                }
            }
            catch (TimeoutException)
            {
                throw new DbUnavailable("Service is unavailable at the moment");
            }
        }

        public async Task<BaseResponse> getAuthorBook(int limit, int offset, string authorId)
        {
            throw new NotImplementedException();
        }
    }
}