﻿using System;
using System.Net;

namespace NetTests.statuses
{
    public class DbUnavailable : StatusesException
    {
        public DbUnavailable(string message) : base(message)
        {
            code = HttpStatusCode.ServiceUnavailable;
        }
    }
}