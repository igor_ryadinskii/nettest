﻿using System;
using System.Net;

namespace NetTests.statuses
{
    public class NotFoundException : StatusesException
    {    
        public NotFoundException(string message) : base(message)
        {
            code = HttpStatusCode.NotFound;
        }
    }
}