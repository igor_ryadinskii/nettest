﻿using System;
using System.Net;

namespace NetTests.statuses
{
    public class StatusesException : Exception
    {
        public HttpStatusCode code;
        public StatusesException(string message) : base(message) { }
    }
}