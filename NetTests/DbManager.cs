﻿using NetTests.domains;
using NetTests.service;

namespace NetTests
{
    public enum DbType
    {
        DB_MONGO,
        DB_POSTGRES
    }

    public class DbManager
    {
        private static DbType currentDbType = DbType.DB_MONGO;

        public static void setDataBase(DbType type)
        {
            currentDbType = type;
        }

        public static DbType getDataBase()
        {
            return currentDbType;
        }

        public static IDbService<AuthorDTO> getCurrentDbServiceAuthor()
        {
            switch (currentDbType)
            {
                case DbType.DB_POSTGRES: return new SqlAuthorService();
                case DbType.DB_MONGO: return new MongoAuthorService();
                default: return new SqlAuthorService();
            }
        }

        public static IDbService<BookDTO> getCurrentDbServiceBook()
        {
            switch (currentDbType)
            {
                case DbType.DB_POSTGRES: return new SqlBookService();
                case DbType.DB_MONGO: return new MongoBookService();
                default: return new SqlBookService();
            }
        }
    }
}