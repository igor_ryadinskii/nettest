﻿using System.Threading.Tasks;
using System.Web.Http;

namespace NetTests.Controllers
{
    public class DatabaseController : ApiController
    {
        [Route("api/database/update/{id}")]
        [HttpPost]
        public async Task<bool> changeDataBase(DbType id)
        {
            return await Task<bool>.Factory.StartNew(() =>
            {
                try
                {
                    DbManager.setDataBase(id);
                    return true;
                }
                catch
                {
                    return false;
                }
            });
        }

        [Route("api/database")]
        public async Task<DbType>  getCurrentDataBase()
        {
            return await Task<DbType>.Factory.StartNew(() =>
            {
                return DbManager.getDataBase();
            });
        }

    }
}
