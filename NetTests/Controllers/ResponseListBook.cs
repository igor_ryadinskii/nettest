﻿using NetTests.domains;
using System.Collections.Generic;

namespace NetTests.Controllers
{
    public class ResponseListBook : BaseResponse
    {
        public IEnumerable<BookDTO> entities { get; set;}
    }
}