﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetTests.Controllers;
using NetTests.domains;
using System.Collections.Generic;

namespace NetTests.Tests
{
    [TestClass]
    public class AuthorControllerTest
    {
        [TestMethod]
        public void addAuthor()
        {
            AuthorController controller = new AuthorController();
            List<AuthorDTO> authorList = new List<AuthorDTO>()
            {
                new AuthorDTO() { familyName = "Фамилия 1" },
                new AuthorDTO() { familyName = "Фамилия 2" },
                new AuthorDTO() { familyName = "Фамилия 3" }
            };
            bool result = controller.addListOfAuthors(authorList).Result;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void getAuthorList()
        {
            AuthorController controller = new AuthorController();
            ResponseListAuthor response = controller.getAllAuthors(1, 0).Result;
            Assert.IsNotNull(response);
            List<AuthorDTO> list = (List<AuthorDTO>)response.entities;
            Assert.IsTrue(list.Count < 2 ? true : false);
        }

        [TestMethod]
        public void getAuthorById()
        {
            AuthorController controller = new AuthorController();
            ResponseListAuthor response = controller.getAllAuthors(1, 0).Result;
            List<AuthorDTO> list = (List<AuthorDTO>)response.entities;
            AuthorDTO author = list[0];
            AuthorDTO authorById = controller.getAuthorById(author.id).Result;
            Assert.AreEqual(author.id, authorById.id);
        }

        [TestMethod]
        public void updateAuthor()
        {
            AuthorController controller = new AuthorController();
            ResponseListAuthor response = controller.getAllAuthors(1, 0).Result;
            List<AuthorDTO> list = (List<AuthorDTO>)response.entities;
            AuthorDTO author = list[0];
            author.familyName = "familyTest";
            author.firstName = "firstTest";
            author.secondName = "secondTest";
            Assert.IsTrue(controller.updateAuthor(author, author.id).Result);
        }

        [TestMethod]
        public void deleteAuthor()
        {
            AuthorController controller = new AuthorController();
            ResponseListAuthor response = controller.getAllAuthors(1, 0).Result;
            List<AuthorDTO> list = (List<AuthorDTO>)response.entities;
            AuthorDTO author = list[0];
            Assert.IsTrue(controller.deleteAuthorById(author.id).Result);
            Assert.IsNull(controller.getAuthorById(author.id).Result);
        }

        [TestMethod]
        public void authorsBooks()
        {
            AuthorController controller = new AuthorController();
            Assert.IsTrue(controller.addListOfAuthors(new List<AuthorDTO>()
            {
                new AuthorDTO()
                {
                    familyName = "bookListTest",
                    firstName = "bookListTest",
                    secondName = "bookListTest"
                }
            }).Result);
            ResponseListAuthor response = controller.getAllAuthors(1, 0).Result;
            List<AuthorDTO> list = (List<AuthorDTO>)response.entities;
            AuthorDTO author = list[0];
            BookController bookController = new BookController();
            BookDTO bookDto = new BookDTO()
            {
                author = author,
                pageCount = 555,
                publicYear = 2013,
                title = "titile"

            };
            Assert.IsTrue(bookController.addListOfBooks(new List<BookDTO>() { bookDto, bookDto, bookDto }).Result);
            List<BookDTO> books = (List<BookDTO>)controller.getAllAuthorBooks(author.id, 10, 0).Result.entities;
            Assert.IsTrue(books.Count == 3 ? true: false);
        }
    }
}
