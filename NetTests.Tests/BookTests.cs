﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetTests.Controllers;
using NetTests.domains;
using System.Collections.Generic;

namespace NetTests.Tests
{
    [TestClass]
    public class BookControllerTest
    {
        [TestMethod]
        public void addBook()
        {
            AuthorController authorController = new AuthorController();
            ResponseListAuthor response = authorController.getAllAuthors(1, 0).Result;
            List<AuthorDTO> list = (List<AuthorDTO>)response.entities;
            AuthorDTO author = list[0];

            BookController controller = new BookController();
            List<BookDTO> bookList = new List<BookDTO>()
            {
                new BookDTO()
                {
                    author = author,
                    pageCount = 555,
                    publicYear = 2013,
                    title = "titile"
                },
                new BookDTO()
                {
                    author = author,
                    pageCount = 755,
                    publicYear = 2015,
                    title = "title2"
                },
                new BookDTO()
                {
                    author = author,
                    pageCount = 5578,
                    publicYear = 2010,
                    title = "title3"
                }
            };
            bool result = controller.addListOfBooks(bookList).Result;
            Assert.IsTrue(result);
        }

        [TestMethod]
        public void getBookList()
        {
            BookController controller = new BookController();
            ResponseListBook response = controller.getAllBooks(1, 0).Result;
            Assert.IsNotNull(response);
            List<BookDTO> list = (List<BookDTO>)response.entities;
            Assert.IsTrue(list.Count < 2 ? true : false);
        }

        [TestMethod]
        public void getBookById()
        {
            BookController controller = new BookController();
            ResponseListBook response = controller.getAllBooks(1, 0).Result;
            List<BookDTO> list = (List<BookDTO>)response.entities;
            BookDTO book = list[0];
            BookDTO bookById = controller.getBookById(book.id).Result;
            Assert.AreEqual(book.id, bookById.id);
        }

        [TestMethod]
        public void updateBook()
        {
            BookController controller = new BookController();
            ResponseListBook response = controller.getAllBooks(1, 0).Result;
            List<BookDTO> list = (List<BookDTO>)response.entities;
            BookDTO book = list[0];
            book.pageCount = 1;
            book.publicYear = 1990;
            book.title = "New title";
            Assert.IsTrue(controller.updateBook(book, book.id).Result);
        }

        [TestMethod]
        public void deleteBook()
        {
            BookController controller = new BookController();
            ResponseListBook response = controller.getAllBooks(1, 0).Result;
            List<BookDTO> list = (List<BookDTO>)response.entities;
            BookDTO book = list[0];
            Assert.IsTrue(controller.deleteBookById(book.id).Result);
            Assert.IsNull(controller.getBookById(book.id).Result);
        }
    }
}
