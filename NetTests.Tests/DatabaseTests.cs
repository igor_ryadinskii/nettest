﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetTests.Controllers;

namespace NetTests.Tests
{
    [TestClass]
    public class DatabaseTest
    {
        [TestMethod]
        public void switchDatabase()
        {
            DatabaseController controller = new DatabaseController();
            Assert.IsTrue(controller.changeDataBase(DbType.DB_POSTGRES).Result);
            Assert.IsTrue(controller.getCurrentDataBase().Result == DbType.DB_POSTGRES);
            Assert.IsTrue(controller.changeDataBase(DbType.DB_MONGO).Result);
            Assert.IsTrue(controller.getCurrentDataBase().Result == DbType.DB_MONGO);
        }

    }
}
