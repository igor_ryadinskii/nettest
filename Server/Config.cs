﻿using Newtonsoft.Json.Serialization;
using Owin;
using System.Net.Http.Headers;
using System.Web.Http;

namespace Server
{
    public class Config
    {
        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configuration(IAppBuilder appBuilder)
        {
            // Configure Web API for self-host. 
            HttpConfiguration config = new HttpConfiguration();
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("application/json"));
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{update}/{id}",
                defaults: new { id = RouteParameter.Optional, update = RouteParameter.Optional }
            );

            appBuilder.UseWebApi(config);
        }
    }
}
