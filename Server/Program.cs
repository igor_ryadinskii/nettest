﻿using Microsoft.Owin.Hosting;
using System;
using System.Net;
using System.Net.Sockets;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            string baseAddress = "http://"+ GetLocalIPAddress() + ":9000/";

            WebApp.Start<Config>(url: baseAddress);
            Console.WriteLine("Server start {0}",baseAddress);
            Console.ReadKey();
        }

        static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }
    }
}
