﻿using NetTests.domains;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;
using NetTests;
using NetTests.Controllers;
using System.Web.Http.Description;
using System.Net.Http;
using System.Net;
using NetTests.statuses;
using System;

namespace Server.Controllers
{
    public class AuthorController : ApiController
    {

        [ResponseType(typeof(ResponseListBook))]
        [Route("api/author/books/{id}")]
        public async Task<HttpResponseMessage> getAllAuthorBooks(HttpRequestMessage request, string id, [FromUri] int limit, [FromUri] int offset)
        {
            try
            {
            return request.CreateResponse(HttpStatusCode.OK, (ResponseListBook)await DbManager.getCurrentDbServiceBook().getAuthorBook(limit, offset, id));
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [ResponseType(typeof(ResponseListAuthor))]
        [Route("api/author")]
        public async Task<HttpResponseMessage> getAllAuthors(HttpRequestMessage request, [FromUri] int limit, [FromUri] int offset)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, (ResponseListAuthor)await DbManager.getCurrentDbServiceAuthor().getAll(limit, offset));
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [ResponseType(typeof(AuthorDTO))]
        [Route("api/author/get/{id}")]
        public async Task<HttpResponseMessage> getAuthorById(HttpRequestMessage request, string id)
        {
            try
            {
                AuthorDTO author = await DbManager.getCurrentDbServiceAuthor().FindById(id);
                return request.CreateResponse(HttpStatusCode.OK, author);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [Route("api/author")]
        [HttpPut]
        public async Task<HttpResponseMessage> addListOfAuthors(HttpRequestMessage request, List<AuthorDTO> authors)
        {
            try
            {
                await DbManager.getCurrentDbServiceAuthor().Insert(authors);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [Route("api/author/delete/{id}")]
        [HttpDelete]
        public async Task<HttpResponseMessage> deleteAuthorById(HttpRequestMessage request, string id)
        {
            try
            {
                await DbManager.getCurrentDbServiceAuthor().Delete(id);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [Route("api/author/update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> updateAuthor(HttpRequestMessage request, AuthorDTO author, string id)
        {
            try
            {
                await DbManager.getCurrentDbServiceAuthor().Update(author, id);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }
    }
}
