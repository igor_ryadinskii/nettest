﻿using NetTests.domains;
using NetTests.statuses;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace NetTests.Controllers
{
    public class BookController : ApiController
    {
        [ResponseType(typeof(ResponseListBook))]
        [Route("api/book")]
        public async Task<HttpResponseMessage> getAllBooks(HttpRequestMessage request, [FromUri] int limit, [FromUri] int offset)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK,(ResponseListBook)await DbManager.getCurrentDbServiceBook().getAll(limit, offset));
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [ResponseType(typeof(BookDTO))]
        [Route("api/book/{id}")]
        public async Task<HttpResponseMessage> getBookById(HttpRequestMessage request, string id)
        {
            try
            {
                return request.CreateResponse(HttpStatusCode.OK, await DbManager.getCurrentDbServiceBook().FindById(id));
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }


        [Route("api/book")]
        [HttpPut]
        public async Task<HttpResponseMessage> addListOfBooks(HttpRequestMessage request, List<BookDTO> books)
        {
            try
            {
                await DbManager.getCurrentDbServiceBook().Insert(books);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [Route("api/book/delete/{id}")]
        [HttpDelete]
        public async Task<HttpResponseMessage> deleteBookById(HttpRequestMessage request, string id)
        {
            try
            {
                await DbManager.getCurrentDbServiceBook().Delete(id);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }

        [Route("api/book/update/{id}")]
        [HttpPost]
        public async Task<HttpResponseMessage> updateBook(HttpRequestMessage request, BookDTO book, string id)
        {
            try
            {
                await DbManager.getCurrentDbServiceBook().Update(book, id);
                return request.CreateResponse(HttpStatusCode.OK);
            }
            catch (StatusesException e)
            {
                HttpResponseMessage response = new HttpResponseMessage(e.code);
                response.Content = new StringContent(e.Message);
                return response;
            }
            catch (Exception ex)
            {
                HttpResponseMessage response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
                response.Content = new StringContent(ex.Message);
                return response;
            }
        }
    }
}
