﻿namespace Client.Model
{
    public class AuthorDTO : IEntity
    {
        public string id { get; set; }
        public string firstName { get; set; }
        public string secondName { get; set; }
        public string familyName { get; set; }
    }
}