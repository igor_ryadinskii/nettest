﻿
namespace Client.Model
{
    public class BookDTO : IEntity
    {
        public string id { get; set; }
        public int publicYear { get; set; }
        public int pageCount { get; set; }
        public string title { get; set; }
        public AuthorDTO author { get; set; }
    }
}