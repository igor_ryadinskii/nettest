﻿
using System;

namespace Client
{
    public class SettingsController
    {
        private static SettingsController instance;

        private SettingsController(){}

        public static SettingsController getInstance()
        {
            if (instance == null)
                instance = new SettingsController();
            return instance;
        }

        public int getCurrentDb()
        {
            return Int32.Parse(Api.requestGET("/api/database/current"));
        }

        public bool setDataBase(DbType type)
        {
            return Api.requestOthers("/api/database/update/" + (int)type, "POST","");
        }
    }
}
