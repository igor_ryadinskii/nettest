﻿using Newtonsoft.Json;
using Client.Model;
using System.Collections.Generic;
using System;
using Client.Controllers;
using Client.statuses;

namespace Client
{
    public class AuthorController : IController<AuthorDTO>
    {
        private static AuthorController instance;

        private AuthorController(){}

        public static AuthorController getInstance()
        {
            if (instance == null)
                instance = new AuthorController();
            return instance;
        }

        public void AddList(List<AuthorDTO> entity)
        {
            try
            {
                Api.requestOthers("/api/author", "PUT", JsonConvert.SerializeObject(entity));
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public void Delete(string Id)
        {
            try
            {
            Api.requestOthers("/api/author/delete/"+Id,"DELETE","");
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public AuthorDTO FindById(string Id)
        {
            throw new NotImplementedException();
        }

        public BaseResponse List(int limit, int offset)
        {
            try
            {
            string result =  Api.requestGET(string.Format("/api/author?limit={0}&offset={1}",limit,offset));
            return JsonConvert.DeserializeObject<ResponseListAuthor>(result);
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public BaseResponse List(int limit, int offset, string authorId)
        {
            try
            {
            string result = Api.requestGET(string.Format("/api/author/book/{0}?limit={1}&offset={2}", authorId ,limit, offset));
            return JsonConvert.DeserializeObject<ResponseListBook>(result);
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(AuthorDTO entity, string id)
        {
            try
            {
            Api.requestOthers("/api/author/update/" + id, "POST", JsonConvert.SerializeObject(entity));
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
