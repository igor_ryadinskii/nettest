﻿using Newtonsoft.Json;
using Client.Model;
using System.Collections.Generic;
using System;
using Client.Controllers;
using Client.statuses;

namespace Client
{
    public class BookController : IController<BookDTO>
    {
        private static BookController instance;

        private BookController(){}

        public static BookController getInstance()
        {
            if (instance == null)
                instance = new BookController();
            return instance;
        }

        public void AddList(List<BookDTO> entity)
        {
            try
            {
            Api.requestOthers("/api/book","PUT", JsonConvert.SerializeObject(entity));
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Delete(string Id)
        {
            try
            {
            Api.requestOthers("/api/book/delete/"+Id,"DELETE","");
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public BookDTO FindById(string Id)
        {
            throw new NotImplementedException();
        }

        public BaseResponse List(int limit, int offset)
        {
            try
            {
                string result = Api.requestGET(string.Format("/api/book?limit={0}&offset={1}", limit, offset));
                return JsonConvert.DeserializeObject<ResponseListBook>(result);
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public void Update(BookDTO entity, string id)
        {
            try
            {
            Api.requestOthers("/api/book/update/" + id, "POST", JsonConvert.SerializeObject(entity));
            }
            catch (StatusesException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

    }
}
