﻿using Client.Controllers;
using Client.Model;
using System.Collections.Generic;

namespace Client
{
    public interface IController<T> where T : class
    {
        BaseResponse List(int limit, int offset);
        void AddList(List<T> entity);
        void Delete(string Id);
        void Update(T entity, string id);
        T FindById(string Id);
    }
}