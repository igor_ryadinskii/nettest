﻿using Client.Model;
using System.Collections.Generic;

namespace Client.Controllers
{
    public class ResponseListAuthor : BaseResponse
    {
        public IEnumerable<AuthorDTO> entities { get; set;}
    }
}