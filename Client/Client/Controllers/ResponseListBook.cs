﻿using Client.Model;
using System.Collections.Generic;

namespace Client.Controllers
{
    public class ResponseListBook : BaseResponse
    {
        public IEnumerable<BookDTO> entities { get; set;}
    }
}