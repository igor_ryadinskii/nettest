﻿using Client.statuses;
using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public class Api
    {

        public static string requestGET(string url)
        {
            HttpResponseMessage response = null;
            String requestString = String.Format("http://{0}:{1}{2}", SettingsData.url, SettingsData.port, url);
            //    try
            //   {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(15);
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            response = client.GetAsync(requestString).Result;

            var responseString = response.Content.ReadAsStringAsync().Result;
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new NotFoundException(responseString);
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                throw new DbUnavailable(responseString);
            return responseString;

            /* }

             catch (Exception e)
             {
                 throw new TimeoutException("Ошибка подключения к серверу");
             }*/
        }

        public static bool requestOthers(string url, string type, string jsonData)
        {
            String requestString = String.Format("http://{0}:{1}{2}", SettingsData.url, SettingsData.port, url);
            //try
            // {
            var client = new HttpClient();
            client.Timeout = TimeSpan.FromSeconds(15);
            client.DefaultRequestHeaders.Add("Accept", "application/json");
            HttpResponseMessage response = null;
            HttpContent content = new StringContent(jsonData, Encoding.UTF8, "application/json");
            switch (type)
            {
                case "PUT":
                    response = client.PutAsync(requestString, content).Result;
                    break;
                case "POST":
                    response = client.PostAsync(requestString, content).Result;
                    break;
                case "DELETE":
                    response = client.DeleteAsync(requestString).Result;
                    break;
            }

            var responseString = response.Content.ReadAsStringAsync().Result;
            if (response.StatusCode == HttpStatusCode.NotFound)
                throw new NotFoundException(responseString);
            if (response.StatusCode == HttpStatusCode.ServiceUnavailable)
                throw new DbUnavailable(responseString);
            return true;

            // }
            //  catch (Exception e)
            //  {
            //     return false;
            // }
        }
    }
}
