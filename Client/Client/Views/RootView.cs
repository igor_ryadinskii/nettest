﻿using System.Collections.Generic;
using Xamarin.Forms;

namespace Client.Views
{
    public class RootView : TabbedPage
    {
        private AuthorView authorView;
        private BookView bookView;
        private SettingsView settingsView;

        public RootView()
        {
            authorView = new AuthorView() { Title = "Авторы"};
            bookView = new BookView() { Title = "Книги" };
            settingsView = new SettingsView() { Title = "Настройки" };
            Children.Add(authorView);
            Children.Add(bookView);
            Children.Add(settingsView);
            CurrentPageChanged += viewChanged;
        }

        private void viewChanged(object sender, System.EventArgs e)
        {
            if (CurrentPage != null)
            {
                foreach (Tab tabPage in Children)
                    if (tabPage.Id == CurrentPage.Id)
                    {
                        tabPage.Checked();
                        return;
                    }
            }
        }
    }
}
