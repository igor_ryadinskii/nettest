﻿using Xamarin.Forms;

namespace Client.Views
{
    public abstract class Tab : CarouselPage
    {
        public abstract void Checked();

        public override string ToString()
        {
            return Title;
        }
    }
}
