﻿using System;
using Xamarin.Forms;

namespace Client.Views
{
    class Paginator : StackLayout
    {
        public int currentPage { get; set; }
        public int allPage { set; get; }
        public long allCount { set
            {
                if(limit == 0 || value == 0)
                {
                    currentPage = 1;
                    offset = (currentPage - 1) * limit;
                    allPage = 1;
                    return;
                }
                allPage = (int) value / limit;
                if(value != (limit * allPage))
                    allPage++;
                if (currentPage > allPage)
                {
                    currentPage = 1;
                    offset = (currentPage - 1) * limit;
                }
                Device.BeginInvokeOnMainThread(() =>
                {
                    labelPages.Text = string.Format("{0}/{1}", currentPage, allPage);
                });
            } }
        public int limit { get; set; }
        public int offset { get; set;}
        private Picker itemsPerPage;
        public event EventHandler OnChangePaging;
        private Label labelPages;

        public Paginator()
        {
            currentPage = 1;
            allCount = 5;
            allPage = 1;
            limit = 5;
            offset = 0;
            Orientation = StackOrientation.Horizontal;
            HorizontalOptions = LayoutOptions.CenterAndExpand;
            InitView();
        }

        private void InitView()
        {
            labelPages = new Label();
            Button buttonPrev;
            Button buttonNext;
            itemsPerPage = new Picker()
            {
                Items = { "5", "10", "15" },
            };

            itemsPerPage.SelectedIndexChanged += (sender, e) =>
            {
                currentPage = 1;
                genarateLimitOffset();
            };

            itemsPerPage.SelectedIndex = 0;

            buttonPrev = new Button()
            {
                Text = "<",
               // IsEnabled = false
            };

            buttonPrev.Clicked += (sender, e) =>
            {
            if (currentPage > 1)
                {
                    currentPage--;

                    genarateLimitOffset();
                }
                else
                {
                   // buttonPrev.IsEnabled = false;
                }
            };

            buttonNext = new Button()
            {
                Text = ">"
            };

            buttonNext.Clicked += (sender, e) =>
            {
                if (currentPage < allPage)
                {
                    currentPage++;
                   // buttonPrev.IsEnabled = true;
                    genarateLimitOffset();
                }
               // else buttonNext.IsEnabled = false;
            };
            
            Children.Add(itemsPerPage);
            Children.Add(buttonPrev);
            Children.Add(labelPages);
            Children.Add(buttonNext);
        }

        private void genarateLimitOffset()
        {
            limit = Int32.Parse(itemsPerPage.Items[itemsPerPage.SelectedIndex]);
            offset = (currentPage - 1) * limit;
            if (OnChangePaging != null)
                OnChangePaging(this, new EventArgs());
            labelPages.Text = string.Format("{0}/{1}",currentPage,allPage);
        }
    }
}
