﻿using Client.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Book
{
    public class BookList : ContentPage
    {
        private BookController controller;
        private TableSection bookTableSection;
        private ActivityIndicator indicator;
        private Paginator paginator;

        public BookList()
        {
            controller = BookController.getInstance();
            paginator = new Paginator();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };
            InitView();
        }

        private void InitView()
        {
            Button updateBtn = new Button() { Text = "Обновить" };
            updateBtn.Clicked += updateBookList;

            StackLayout bookLayoutList = new StackLayout() { Orientation = StackOrientation.Vertical };

            bookTableSection = new TableSection("Список книг");
            TableView bookListView = new TableView()
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    bookTableSection
                }
            };
            ScrollView bookListScroll = new ScrollView()
            {
                Content = bookListView
            };
            bookLayoutList.Children.Add(updateBtn);
            bookLayoutList.Children.Add(indicator);
            bookLayoutList.Children.Add(bookListScroll);
            bookLayoutList.Children.Add(paginator);
            Content = bookLayoutList;

        }

        public void updateBookList(object sender, EventArgs e)
        {
            if (indicator.IsRunning)
                return;
            indicator.IsRunning = true;
            indicator.IsVisible = true;
            bookTableSection.Clear();
            Task.Run(() =>
            {
                try
                {
                    ResponseListBook responseBook = (ResponseListBook)controller.List(paginator.limit, paginator.offset);
                    List<Model.BookDTO> books = (List<Model.BookDTO>)responseBook.entities;
                    if (books == null || books?.Count==0)
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            bookTableSection.Add(new ViewCell() { View = new Label() { Text = "Список пуст" } });
                        });
                    else
                        foreach (Model.BookDTO book in books)
                        {

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ViewCell cell = new ViewCell();
                                cell.Tapped += bookEdit;
                                BookItem item = new BookItem(book, true);
                                item.OnClick += deleteBook;
                                cell.View = item;
                                bookTableSection.Add(cell);
                            });
                        }

                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Ошибка", ex.Message, "Ok");
                    });
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        indicator.IsRunning = false;
                    });
                }
            });
        }

        private async void bookEdit(object sender, EventArgs e)
        {
            ViewCell cell = (ViewCell)sender;
            BookItem item = ((BookItem)cell.View);
            await Navigation.PushModalAsync(new BookEdit(this,item.book));
        }
        
        private void deleteBook(object sender, Model.BookDTO e)
        {
            string id = e.id;
            
            indicator.IsRunning = true;
            indicator.IsVisible = true;
            Task.Run(()=>
            {
                try
                {
                    controller.Delete(id);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Удаление", "Книга удалена", "OK");
                        indicator.IsRunning = false;
                        updateBookList(this, null);
                    });
                }
                catch
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Удаление", "Ушибка удаления", "OK");
                    });
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        indicator.IsRunning = false;
                    });
                }
            });
            
        }
    }
}
