﻿using Client.Controllers;
using Client.statuses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Book
{
    public class BookEdit : ContentPage
    {
        private BookController controller;
        private ActivityIndicator indicator;
        private EntryCell publicYear;
        private EntryCell pageCount;
        private EntryCell title;
        private Model.BookDTO book;
        private Picker authors;
        private Dictionary<int, Model.AuthorDTO> authorsDictionary;
        private BookList bookList;

        public BookEdit(BookList bl, Model.BookDTO book)
        {
            this.book = book;
            this.bookList = bl;
            controller = BookController.getInstance();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };
            authors = new Picker() { Title = "Выберите автора" };
            authorsDictionary = new Dictionary<int, Model.AuthorDTO>();
            updateAuthorsList();
            InitView();
        }

        public void updateAuthorsList()
        {
            Task.Run(() =>
            {
                authorsDictionary.Clear();
                Device.BeginInvokeOnMainThread(() =>
                {
                    authors.Items.Clear();
                });
                ResponseListAuthor authorsResponse = (ResponseListAuthor)AuthorController.getInstance().List(Int32.MaxValue, 0);
                List<Model.AuthorDTO> listOfAllAuthor = (List<Model.AuthorDTO>)authorsResponse.entities;
                for (int i = 0; i < listOfAllAuthor.Count; i++)
                {
                    Model.AuthorDTO author = listOfAllAuthor[i];
                    authorsDictionary.Add(i, author);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        authors.Items.Add(string.Format("{0} {1} {2}", author.familyName, author.firstName, author.secondName));
                    });
                    if (author.id == book.id)
                        authors.SelectedIndex = i;
                }
            });
        }

        private void InitView()
        {
            publicYear = new EntryCell() { Label = "Год публикации", Text = book.publicYear.ToString(), Keyboard = Keyboard.Numeric };
            pageCount = new EntryCell() { Label = "Количество страниц", Text = book.pageCount.ToString(), Keyboard = Keyboard.Numeric };
            title = new EntryCell() { Label = "Название", Text = book.title};

            Button addBookButton = new Button()
            {
                Text = "Обновить книгу"
            };

            addBookButton.Clicked += (sender, e) =>
            {
                indicator.IsRunning = true;
                indicator.IsVisible = true;
                
                Task.Run(() =>
                {
                    try
                    {
                        string id= book.id;
                        book.title = title.Text;
                        book.publicYear = Int32.Parse(publicYear.Text);
                        book.pageCount = Int32.Parse(pageCount.Text);
                        book.author = authorsDictionary[authors.SelectedIndex];
                        controller.Update(book, id);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            bookList.updateBookList(this,null);
                            DisplayAlert("Успех", "данные обновлены. ", "OK");
                        });
                    }
                    catch (StatusesException se)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не обновлены. " + se.Message, "OK");
                        });
                    }
                    catch (Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не обновлены. " + ex.Message, "OK");
                        });
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            indicator.IsRunning = false;
                        });
                    }
                });
            };

            TableView table = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    
                    new TableSection ("Редактирование книги")
                    {
                        title,
                        publicYear,
                        pageCount,
                        new ViewCell
                        {
                            View = new StackLayout
                            {
                                Orientation = StackOrientation.Horizontal,
                                Children =
                                            {
                                                new Label
                                                {
                                                    Text = "Автор",
                                                    FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
                                                },
                                                authors
                                            }
                            }
                        },
                    }
                }
            };
            StackLayout layout = new StackLayout() { Orientation = StackOrientation.Vertical };
            layout.Children.Add(table);
            layout.Children.Add(indicator);
            layout.Children.Add(addBookButton);
            Content = layout;
        }
    }
}
