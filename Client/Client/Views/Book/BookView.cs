﻿using Client.Views.Book;
using System;
using Xamarin.Forms;

namespace Client.Views
{
    public class BookView : Tab
    {
        private BookList bookList;
        private BookAdd bookAdd;
        public BookView()
        {
            InitView();
        }

        public override void Checked()
        {
            bookList.updateBookList(this, null);
        }

        private void InitView()
        {
            bookList = new BookList();
            bookAdd = new BookAdd();

            CurrentPageChanged += OnPageSwipe;

            /* Add swipes pages */
            Children.Add(bookList);
            Children.Add(bookAdd);
        }

        private void OnPageSwipe(object sender, System.EventArgs e)
        {
            if (CurrentPage != null)
            {
                if (bookList.Id == CurrentPage.Id)
                    Checked();
                else 
                if (bookAdd.Id == CurrentPage.Id)
                {
                    bookAdd.updateAuthorsList();
                }
            }
        }
    }
}