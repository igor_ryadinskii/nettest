﻿using Client.Controllers;
using Client.statuses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Book
{
    public class BookAdd : ContentPage
    {
        private BookController controller;
        private ActivityIndicator indicator;
        private EntryCell publicYear;
        private EntryCell pageCount;
        private EntryCell title;
        private Picker authors;
        private Dictionary<int, Model.AuthorDTO> authorsDictionary;

        public BookAdd()
        {
            controller = BookController.getInstance();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };
            authors = new Picker() { Title = "Выберите автора" };
            authorsDictionary = new Dictionary<int, Model.AuthorDTO>();
 
            InitView();
        }

        public void updateAuthorsList()
        {
            Task.Run(() =>
            {
                try
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        authors.Items.Clear();
                    });
                    authorsDictionary.Clear();
                    ResponseListAuthor authorsResponse = (ResponseListAuthor)AuthorController.getInstance().List(Int32.MaxValue, 0);
                    List<Model.AuthorDTO> listOfAllAuthor = (List<Model.AuthorDTO>)authorsResponse.entities;
                    if (listOfAllAuthor == null)
                    {
                        DisplayAlert("ListOfAuthors", "null", "Ok");
                    }
                    else
                        for (int i = 0; i < listOfAllAuthor.Count; i++)
                        {
                            Model.AuthorDTO author = listOfAllAuthor[i];
                            authorsDictionary.Add(i, author);
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                authors.Items.Add(string.Format("{0} {1} {2}", author.familyName, author.firstName, author.secondName));
                            });
                        }
                }
                catch { }
            });
        }

        private void InitView()
        {
            publicYear = new EntryCell() { Label = "Год публикации", Keyboard = Keyboard.Numeric };
            pageCount = new EntryCell() { Label = "Количество страниц", Keyboard = Keyboard.Numeric };
            title = new EntryCell() { Label = "Название"};

            Button addBookButton = new Button() { Text = "Добавить книгу" };

            addBookButton.Clicked += (sender, e) =>
            {
                indicator.IsRunning = true;
                indicator.IsVisible = true;
                
                Task.Run(() =>
                {
                    try
                    {
                        Model.BookDTO book = new Model.BookDTO();
                        book.title = title.Text;
                        book.publicYear = Int32.Parse(publicYear.Text);
                        book.pageCount = Int32.Parse(pageCount.Text);
                        book.author = authorsDictionary[authors.SelectedIndex];
                        controller.AddList(new List<Model.BookDTO>() { book });
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            title.Text = "";
                            publicYear.Text = "";
                            pageCount.Text = "";
                            DisplayAlert("Успех", "Данные добавлены. ", "OK");
                        });
                    }
                    catch (StatusesException se)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не добавлены. " + se.Message, "OK");
                        });
                    }
                    catch (Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не добавлены. " + ex.Message, "OK");
                        });
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            indicator.IsRunning = false;
                        });
                    }
                });
            };

            TableView table = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    
                    new TableSection ("Добавление книги")
                    {
                        title,
                        pageCount,
                        publicYear,
                        new ViewCell
                        {
                            View = new StackLayout
                            {
                                Orientation = StackOrientation.Horizontal,
                                Children =
                                            {
                                                new Label
                                                {
                                                    Text = "Автор",
                                                    FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
                                                },
                                                authors
                                            }
                            }
                        },
                    }
                }
            };
            StackLayout layout = new StackLayout() { Orientation = StackOrientation.Vertical };
            layout.Children.Add(table);
            layout.Children.Add(indicator);
            layout.Children.Add(addBookButton);
            Content = layout;
        }
    }
}
