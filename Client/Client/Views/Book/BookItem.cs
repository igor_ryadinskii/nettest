﻿using System;
using Xamarin.Forms;

namespace Client.Views.Book
{
    public class BookItem : Grid
    {
        private Label bookInfo;
        private Button deleteButton;
        public Model.BookDTO book { get; }
        public event EventHandler<Model.BookDTO> OnClick;
        public bool isDeleteEnable = false;

        public BookItem(Model.BookDTO book, bool delete)
        {
            this.book = book;
            this.isDeleteEnable = delete;
            bookInfo = new Label();
            bookInfo.Text = string.Format("{0} {1} {2}c.\n{3}", book.publicYear, book.title, book.pageCount,
                            string.Format("{0} {1} {2}", book.author?.familyName, book.author?.firstName, book.author?.secondName)
                            );
            deleteButton = new Button()
            {
                Text = "X",
                BackgroundColor = Color.Red,
                HorizontalOptions = LayoutOptions.End
            };
            deleteButton.Clicked += (sender, e) =>
            {
                OnClick(deleteButton, book);
            };
            InitView();
        }

        private void InitView()
        {
            Children.Add(bookInfo);
            if(isDeleteEnable)
                Children.Add(deleteButton);
        }

    }
}
