﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Settings
{
    public class SettingsControls : ContentPage
    {
        private EntryCell url;
        private EntryCell port;
        private Picker databases;
        private Dictionary<string, DbType> databasesPicker;
        private SettingsController controller;
        private ActivityIndicator indicator;

        public SettingsControls()
        {
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true
            };
            databasesPicker = new Dictionary<string, DbType>()
            {
                { "Mongo DB", DbType.DB_MONGO},
                { "PostgreSql", DbType.DB_POSTGRES}
            };

            controller = SettingsController.getInstance();
            InitView();
        }

        public void setDb()
        {
            indicator.IsRunning = true;
            indicator.IsVisible = true;
            Task.Run(()=>
            {
                try
                {
                    DbType dbType = (DbType)controller.getCurrentDb();
                    SettingsData.dbType = dbType;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        databases.SelectedIndex = (int)dbType;
                    });
                    indicator.IsRunning = false;
                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Ошибка", ex.Message, "Ok");
                    });
                    indicator.IsRunning = false;
                }
            });
        }

        private void InitView()
        {
            Button applySettingsButton = new Button()
            {
                Text = "Применить"
            };

            applySettingsButton.Clicked += (sender, e)=>
            {
                indicator.IsRunning = true;
                indicator.IsVisible = true;
                SettingsData.url = url.Text;
                SettingsData.port = Int32.Parse(port.Text);
                SettingsData.dbType = databasesPicker[databases.Items[databases.SelectedIndex]];
                Task.Run(() =>
                {
                    try
                    {
                        bool response = controller.setDataBase(SettingsData.dbType);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            if (response)
                                DisplayAlert("Изменение настроек", "Данные сохранены", "OK");
                            else
                                DisplayAlert("Изменение настроек", "Ошибка сохранения", "OK");
                        });
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            indicator.IsRunning = false;
                        });
                    }
                });
            };

            StackLayout layout = new StackLayout() { Orientation = StackOrientation.Vertical };
            databases = new Picker();

            foreach (string authorFIO in databasesPicker.Keys)
                databases.Items.Add(authorFIO);
            databases.SelectedIndex = (int)SettingsData.dbType;

            port = new EntryCell() { Keyboard = Keyboard.Numeric, Text = SettingsData.port.ToString() , Label = "Порт: " };
            url = new EntryCell() { Text = SettingsData.url, Label = "Сервер: "};

            TableView table = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    new TableSection ("Настройки подключения к серверу") {
                        url,
                        port
                    },
                    new TableSection ("Выбор базы данных")
                    {
                        new ViewCell
                        {
                            View = new StackLayout
                            {
                                Orientation = StackOrientation.Horizontal,
                                Children =
                                            {
                                                new Label
                                                {
                                                    Text = "Выбранная база:",
                                                    FontSize = Device.GetNamedSize (NamedSize.Medium, typeof(Label)),
                                                },
                                                databases
                                            }
                            }
                        },
                        new ViewCell
                        {
                            View = indicator
                        }

                    }
                }
            };
            layout.Children.Add(table);
            layout.Children.Add(indicator);
            layout.Children.Add(applySettingsButton);
                
            Content = layout;
        }
    }
}
