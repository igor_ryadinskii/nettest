﻿using Client.Views.Settings;
using Xamarin.Forms;

namespace Client.Views
{
    public class SettingsView : Tab
    {
        private SettingsControls settingsControls;

        public SettingsView()
        {
            settingsControls = new SettingsControls();
            Children.Add(settingsControls);
        }

        public override void Checked()
        {
            settingsControls.setDb();
        }
    }
}
