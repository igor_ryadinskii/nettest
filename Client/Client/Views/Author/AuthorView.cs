﻿using Client.Views.Author;
using Xamarin.Forms;

namespace Client.Views
{
    public class AuthorView : Tab
    {
        private AuthorList authorList;

        public AuthorView()
        {
            InitView();
        }

        public override void Checked()
        {
            authorList.updateAuthorList(this,null);
        }

        private void InitView()
        {
            authorList = new AuthorList();
            /* Add swipes pages */

            CurrentPageChanged += OnPageSwipe;
            Children.Add(authorList);
            Children.Add(new AuthorAdd());
        }

        private void OnPageSwipe(object sender, System.EventArgs e)
        {
            if (CurrentPage != null)
            {
                if (authorList.Id == CurrentPage.Id)
                    Checked();
                else
                {
                    Checked();
                }
            }
        }
    }
}
