﻿using Client.Controllers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Author
{
    public class AuthorList : ContentPage
    {
        private AuthorController controller;
        private TableSection authorTableSection;
        private ActivityIndicator indicator;
        private Paginator paginator;

        public AuthorList()
        {
            controller = AuthorController.getInstance();
            paginator = new Paginator();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };

            paginator.OnChangePaging += updateAuthorList;

            InitView();
        }

        private void InitView()
        {
            Button updateBtn = new Button() { Text = "Обновить" };
            updateBtn.Clicked += updateAuthorList;

            StackLayout authorLayoutList = new StackLayout() { Orientation = StackOrientation.Vertical };

            authorTableSection = new TableSection("Список авторов");
            TableView authorListView = new TableView()
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    authorTableSection
                }
            };
            ScrollView authorListScroll = new ScrollView()
            {
                Content = authorListView
            };

            authorLayoutList.Children.Add(updateBtn);
            authorLayoutList.Children.Add(indicator);
            authorLayoutList.Children.Add(authorListScroll);
            authorLayoutList.Children.Add(paginator);

            Content = authorLayoutList;

        }

        public void updateAuthorList(object sender, EventArgs e)
        {
            if (indicator.IsRunning)
                return;
            indicator.IsRunning = true;
            indicator.IsVisible = true;
            authorTableSection.Clear();
            Task.Run(() =>
            {
                try
                {
                    ResponseListAuthor responseAuthor = (ResponseListAuthor)controller.List(paginator.limit, paginator.offset);
                    IEnumerable<Model.AuthorDTO> authors = responseAuthor?.entities;
                    paginator.allCount = responseAuthor.count; 
                    if (authors == null || responseAuthor?.count ==0 )
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            authorTableSection.Add(new ViewCell() { View = new Label() { Text = "Список пуст" } });
                        });
                    else
                        foreach (Model.AuthorDTO auth in authors)
                        {

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ViewCell cell = new ViewCell();
                                cell.Tapped += authorAction;
                                AuthorItem item = new AuthorItem(auth);
                                item.OnClick += deleteAuthor;
                                cell.View = item;
                                authorTableSection.Add(cell);
                            });
                        }

                }
                catch (Exception ex)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Ошибка", ex.Message, "Ok");
                    });
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        indicator.IsRunning = false;
                    });
                }
            });
        }

        private async void authorAction(object sender, EventArgs e)
        {
            ViewCell cell = (ViewCell)sender;
            AuthorItem item = ((AuthorItem)cell.View);
            string [] authorActions = { "Редактирование", "Список книг" };
            var action = await DisplayActionSheet("Выбрать действие", "Отмена", null, authorActions);
            switch(action)
            {
                case "Редактирование":
                    await Navigation.PushModalAsync(new AuthorEdit(this,item.author));
                    break;

                case "Список книг":
                    await Navigation.PushModalAsync(new AuthorBookList(item.author));
                    break;
            }
        }
        
        private void deleteAuthor(object sender, Model.AuthorDTO e)
        {
            string id = e.id;
            indicator.IsRunning = true;
            indicator.IsVisible = true;
            Task.Run(()=>
            {
                try
                {
                    controller.Delete(id);
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Удаление", "Автор удалён", "OK");
                        indicator.IsRunning = false;
                        updateAuthorList(this, null);
                    });
                }
                catch
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        DisplayAlert("Удаление", "Ушибка удаления", "OK");
                    });
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        indicator.IsRunning = false;
                    });
                }
            });
            
        }
    }
}
