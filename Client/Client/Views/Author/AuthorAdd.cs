﻿using Client.statuses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Author
{
    public class AuthorAdd : ContentPage
    {
        private AuthorController controller;
        private ActivityIndicator indicator;
        private EntryCell firstName;
        private EntryCell secondName;
        private EntryCell lastName;

        public AuthorAdd()
        {
            controller = AuthorController.getInstance();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };
            InitView();
        }

        private void InitView()
        {
            firstName = new EntryCell() { Label = "Имя" };
            secondName = new EntryCell() { Label = "Отчество" };
            lastName = new EntryCell() { Label = "Фамилия" };

            Button addAuthorButton = new Button()
            {
                Text = "Добавить автора"
            };

            addAuthorButton.Clicked += (sender, e) =>
            {
                indicator.IsRunning = true;
                indicator.IsVisible = true;
                
                Task.Run(() =>
                {
                    try
                    {
                        Model.AuthorDTO author = new Model.AuthorDTO();
                        author.firstName = firstName.Text;
                        author.secondName = secondName.Text;
                        author.familyName = lastName.Text;
                        controller.AddList(new List<Model.AuthorDTO>() { author });
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            firstName.Text = "";
                            secondName.Text = "";
                            lastName.Text = "";
                            DisplayAlert("Успех", "Данные добавлены. ", "OK");
                        });
                    }
                    catch (StatusesException se)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не добавлены. " + se.Message, "OK");
                        });
                    }
                    catch (Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не добавлены. " + ex.Message, "OK");
                        });
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            indicator.IsRunning = false;
                        });
                    }
                });
            };

            TableView table = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    
                    new TableSection ("Добалвение автора")
                    {
                        firstName,
                        secondName,
                        lastName
                    }
                }
            };
            StackLayout layout = new StackLayout() { Orientation = StackOrientation.Vertical };
            layout.Children.Add(table);
            layout.Children.Add(indicator);
            layout.Children.Add(addAuthorButton);
            Content = layout;
        }
    }
}
