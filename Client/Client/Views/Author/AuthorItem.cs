﻿using System;
using Xamarin.Forms;

namespace Client.Views.Author
{
    public class AuthorItem : Grid
    {
        Label authorInfo;
        Button deleteButton;
        public Model.AuthorDTO author { get; }
        public event EventHandler<Model.AuthorDTO> OnClick;

        public AuthorItem(Model.AuthorDTO author)
        {
            this.author = author;
            authorInfo = new Label();
            authorInfo.Text = string.Format("{0} {1} {2} ", author.familyName, author.firstName, author.secondName);
            deleteButton = new Button()
            {
                Text = "X",
                BackgroundColor = Color.Red,
                HorizontalOptions = LayoutOptions.End
            };
            deleteButton.Clicked += (sender, e) =>
            {
                OnClick(deleteButton, author);
            };
            InitView();
        }

        private void InitView()
        {
            Children.Add(authorInfo);
            Children.Add(deleteButton);
        }

    }
}
