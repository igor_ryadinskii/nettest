﻿using Client.statuses;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Author
{
    public class AuthorEdit : ContentPage
    {
        private AuthorController controller;
        private ActivityIndicator indicator;
        private EntryCell firstName;
        private EntryCell secondName;
        private EntryCell lastName;
        private Model.AuthorDTO author;
        private AuthorList authorList;


        public AuthorEdit(AuthorList al,Model.AuthorDTO author)
        {
            this.author = author;
            authorList = al;
            controller = AuthorController.getInstance();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };
            InitView();
        }

        private void InitView()
        {
            firstName = new EntryCell() { Label = "Имя", Text = author.firstName };
            secondName = new EntryCell() { Label = "Отчество", Text = author.secondName };
            lastName = new EntryCell() { Label = "Фамилия", Text = author.familyName };

            Button addAuthorButton = new Button()
            {
                Text = "Обновить автора"
            };

            addAuthorButton.Clicked += (sender, e) =>
            {
                indicator.IsRunning = true;
                indicator.IsVisible = true;
                
                Task.Run(() =>
                {
                    try
                    {
                        string id= author.id;
                        author.firstName = firstName.Text;
                        author.secondName = secondName.Text;
                        author.familyName = lastName.Text;
                        controller.Update(author, id);
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            authorList.updateAuthorList(this, null);
                            DisplayAlert("Успех", "данные  обновлены. ", "OK");
                        });
                    }
                    catch(StatusesException se)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не обновлены. "+se.Message, "OK");
                        });
                    }
                    catch(Exception ex)
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            DisplayAlert("Ошибка", "данные не обновлены. " + ex.Message, "OK");
                        });
                    }
                    finally
                    {
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            indicator.IsRunning = false;
                        });
                    }
                });
            };

            TableView table = new TableView
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    
                    new TableSection ("Редактирование автора")
                    {
                        firstName,
                        secondName,
                        lastName
                    }
                }
            };
            StackLayout layout = new StackLayout() { Orientation = StackOrientation.Vertical };
            layout.Children.Add(table);
            layout.Children.Add(indicator);
            layout.Children.Add(addAuthorButton);
            Content = layout;
        }
    }
}
