﻿using Client.Controllers;
using Client.Views.Book;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Client.Views.Author
{
    public class AuthorBookList : ContentPage
    {
        private AuthorController controller;
        private TableSection bookTableSection;
        private Paginator paginator;
        private ActivityIndicator indicator;

        public AuthorBookList(Model.AuthorDTO author)
        {
            controller = AuthorController.getInstance();
            paginator = new Paginator();
            indicator = new ActivityIndicator()
            {
                VerticalOptions = LayoutOptions.Center,
                IsEnabled = true,
                IsRunning = false
            };
            InitView();
            updateBookList(author.id);
        }

        private void InitView()
        {
            
            StackLayout bookLayoutList = new StackLayout() { Orientation = StackOrientation.Vertical };

            bookTableSection = new TableSection("Список книг автора");
            TableView bookListView = new TableView()
            {
                Intent = TableIntent.Form,
                Root = new TableRoot()
                {
                    bookTableSection
                }
            };
            ScrollView bookListScroll = new ScrollView()
            {
                Content = bookListView
            };

            bookLayoutList.Children.Add(indicator);
            bookLayoutList.Children.Add(bookListScroll);
            bookLayoutList.Children.Add(paginator);
            Content = bookLayoutList;

        }

        public void updateBookList(string id)
        {
            if (indicator.IsRunning)
                return;
            indicator.IsRunning = true;
            indicator.IsVisible = true;
            bookTableSection.Clear();
            Task.Run(() =>
            {
                try
                {
                    ResponseListBook responseBook = (ResponseListBook)controller.List(paginator.limit, paginator.offset, id);
                    List<Model.BookDTO> books = (List<Model.BookDTO>)responseBook.entities;
                    if (books == null || books?.Count==0)
                        Device.BeginInvokeOnMainThread(() =>
                        {
                            bookTableSection.Add(new ViewCell() { View = new Label() { Text = "Список пуст" } });
                        });
                    else
                        foreach (Model.BookDTO book in books)
                        {

                            Device.BeginInvokeOnMainThread(() =>
                            {
                                ViewCell cell = new ViewCell();
                                BookItem item = new BookItem(book, false);
                                cell.View = item;
                                bookTableSection.Add(cell);
                            });
                        }

                }
                catch(Exception ex)
                {
                    int g = 0;
                }
                finally
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        indicator.IsRunning = false;
                    });
                }
            });
        }
    }
}
