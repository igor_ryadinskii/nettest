﻿namespace Client
{
    public enum DbType
    {
        DB_MONGO,
        DB_POSTGRES
    }

    public class SettingsData
    {
        public static string url = "192.168.1.157";
        public static int port = 9000;
        public static DbType dbType = DbType.DB_MONGO;
    }
}
